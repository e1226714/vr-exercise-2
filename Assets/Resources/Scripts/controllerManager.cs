﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class controllerManager : MonoBehaviour
{
    private GameObject cue;

    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;

    bool grabbed = false;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    void Start()
    {

    }

    void Update()
    {
       // Debug.Log("Print" + UnityEngine.Input.GetJoystickNames());
        device = SteamVR_Controller.Input((int)trackedObj.index);
        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && cue != null)
        {
            Debug.Log("game object: " + cue.gameObject.tag);
            if (cue.transform.parent == null)
            {

                Debug.Log("pick up: " + cue.gameObject.tag);
                cue.transform.parent = this.transform;

                cue.GetComponent<Rigidbody>().isKinematic = true;

                //device.TriggerHapticPulse(2500);
                grabbed = true;
                rumbleController();
            }
            else if (cue.CompareTag("cue"))
            {
                grabbed = false;

                Debug.Log("lay down: " + cue.gameObject.tag + grabbed);
                cue.transform.parent = null;

                cue.GetComponent<Rigidbody>().isKinematic = false;
                rumbleController();

            }
        }

        if (dashBalls.GetDashedBall())
        {
            rumbleController();
            dashBalls.SetDashedBall(false);
        }
    }

    void FixedUpdate()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("cue"))
        {
            cue = col.gameObject;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        cue = null;
        
    }

    private void OnTriggerStay(Collider col)
    {
        if (!grabbed && col.gameObject.CompareTag("cue")) {
            device.TriggerHapticPulse(300);
        }

    }

    public void rumbleController()
    {
        Debug.Log("rumble");
            StartCoroutine(LongVibration(0.1f, 3999));
    }

    IEnumerator LongVibration(float length, ushort strength)
    {
        for (float i = 0; i < length; i += Time.deltaTime)
        {
            device.TriggerHapticPulse(strength);
            yield return null; //every single frame for the duration of "length" you will vibrate at "strength" amount
        }
    }

}



