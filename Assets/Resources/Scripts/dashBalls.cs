﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class dashBalls : MonoBehaviour
{
    //public SteamVR_ControllerManager SteamVR_Manager;
    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;
    private int power;

    private static bool dashedBall = false;
    // Use this for initialization
    void Start()
    {
        power = 80;
    }

    // Update is called once per frame
    void Update()
    {   
      
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ball") || collision.gameObject.CompareTag("cueball"))
        {
            Vector3 dir = (collision.contacts[0].point - this.transform.position).normalized;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(dir * power);

            dashedBall = true;
        }
    }

    public static bool GetDashedBall()
    {
        return dashedBall;
    }

    public static void SetDashedBall(bool b)
    {
        dashedBall = b;
    }
}