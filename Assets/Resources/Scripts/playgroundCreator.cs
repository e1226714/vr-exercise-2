﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playgroundCreator : MonoBehaviour {

    public GameObject playground;
    public PhysicMaterial bouncines;
    public Material wall, hole;

    public GameObject[] walls = new GameObject[6];
    private Vector3 scale = new Vector3(0.75f, 0.75f, 0.01f);

    public GameObject[] holes = new GameObject[8];
    private Vector3 radius = new Vector3(0.2f, 0.2f, 0.2f);

    // Use this for initialization
    void Start () {

        //create walls
        for (int i = 0; i <= 5; i++)
        {
            walls[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            walls[i].transform.localScale = scale;
            walls[i].GetComponent<Renderer>().material = wall;
            walls[i].name = "wall" + i;
            walls[i].tag = "wall";
            walls[i].transform.SetParent(playground.transform);
            walls[i].GetComponent<BoxCollider>().material = bouncines;
            
        }

        walls[0].transform.position = new Vector3(0, 1.237f, -0.33f);

        walls[1].transform.position = new Vector3(0, 1.237f, 0.413f);

        walls[2].transform.position = new Vector3(-0.375f, 1.237f, 0.035f);
        walls[2].transform.rotation = Quaternion.Euler(0, 90, 0);

        walls[3].transform.position = new Vector3(0.377f, 1.237f, 0.035f);
        walls[3].transform.rotation = Quaternion.Euler(0, 90, 0);

        walls[4].transform.position = new Vector3(0, 1.616f, 0.035f);
        walls[4].transform.rotation = Quaternion.Euler(90, 90, 0);

        walls[5].transform.position = new Vector3(0, 0.87f, 0.035f);
        walls[5].transform.rotation = Quaternion.Euler(90, 90, 0);


        //create holes in corners
        for (int i = 0; i <= 7; i++)
        {
            holes[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            holes[i].transform.localScale = radius;
            holes[i].GetComponent<Renderer>().material = hole;
            holes[i].name = "hole" + i;
            holes[i].tag = "hole";
            holes[i].transform.SetParent(playground.transform);
            holes[i].AddComponent<letBallgothrough>();
            holes[i].GetComponent<SphereCollider>().radius = 0.25f;
        }

        holes[0].transform.position = new Vector3(-0.375f, 1.617f, 0.41f);
        holes[1].transform.position = new Vector3(0.38f, 1.62f, 0.41f);
        holes[2].transform.position = new Vector3(0.38f, 1.62f, -0.333f);
        holes[3].transform.position = new Vector3(-0.375f, 1.62f, -0.333f);
        holes[4].transform.position = new Vector3(-0.375f, 0.861f, -0.333f);
        holes[5].transform.position = new Vector3(-0.375f, 0.861f, 0.41f);
        holes[6].transform.position = new Vector3(0.38f, 0.861f, 0.41f);
        holes[7].transform.position = new Vector3(0.38f, 0.861f, -0.333f);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
