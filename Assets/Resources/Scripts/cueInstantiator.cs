﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cueInstantiator : MonoBehaviour {

    public GameObject cueHandler;
    public GameObject cuePrefab;
    //public Texture2D cueTexture;

	// Use this for initialization
	void Start () {

        GameObject cue = (GameObject)Instantiate(cuePrefab);
        //cue.GetComponent<Renderer>().material.mainTexture = cueTexture;
        cue.transform.position = new Vector3(-0.6f, 1.7f, 0);
        cue.gameObject.tag = "cue";
       // cue.transform.SetParent(cueHandler.transform);
       // cue.AddComponent<dashBalls>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
