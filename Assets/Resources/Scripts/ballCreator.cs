﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballCreator : MonoBehaviour {

    public GameObject playBalls;
    public PhysicMaterial bouncines;
    public Texture2D ball1, ball2, ball3, ball4, ball5, ball6, ball7, ball8, ball9, ball10, 
        ball11, ball12, ball13, ball14, cueBall;

    public GameObject[] balls = new GameObject[15];
    private Vector3 radius = new Vector3(0.06f, 0.06f, 0.06f);

	// Use this for initialization
	void Start () {
        for (int i = 0; i < 15; i++)
        {
            balls[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            balls[i].transform.localScale = radius;
            balls[i].name = "ball" + i;
            balls[i].tag = "ball";
            balls[i].transform.SetParent(playBalls.transform);
            balls[i].AddComponent<Rigidbody>();
            balls[i].GetComponent<Rigidbody>().useGravity = false;
            balls[i].GetComponent<Rigidbody>().isKinematic = false;
            balls[i].GetComponent<SphereCollider>().material = bouncines;
            balls[i].GetComponent<Rigidbody>().angularDrag = 0.5f;
            
        }

        balls[0].transform.localPosition = new Vector3(0, 1, 0);
        balls[1].transform.localPosition = new Vector3(0, 1, 0.07f);
        balls[2].transform.localPosition = new Vector3(0.07f, 1, 0.07f);
        balls[3].transform.localPosition = new Vector3(0.07f, 1, 0);
        balls[4].transform.localPosition = new Vector3(0.07f, 1, 0.14f);
        balls[5].transform.localPosition = new Vector3(0, 1, 0.14f);
        balls[6].transform.localPosition = new Vector3(0.14f, 1, 0.14f);
        balls[7].transform.localPosition = new Vector3(0.14f, 1, 0.07f);
        balls[8].transform.localPosition = new Vector3(0.14f, 1, 0);
        balls[9].transform.localPosition = new Vector3(0.105f, 1.06f, 0.035f);
        balls[10].transform.localPosition = new Vector3(0.105f, 1.06f, 0.105f);
        balls[11].transform.localPosition = new Vector3(0.035f, 1.06f, 0.105f);
        balls[12].transform.localPosition = new Vector3(0.035f, 1.06f, 0.035f);
        balls[13].transform.localPosition = new Vector3(0.07f, 1.12f, 0.07f);
        balls[14].transform.localPosition = new Vector3(0.07f, 1.38f, 0.07f);

        balls[0].GetComponent<Renderer>().material.mainTexture = ball1;
        balls[1].GetComponent<Renderer>().material.mainTexture = ball2;
        balls[2].GetComponent<Renderer>().material.mainTexture = ball3;
        balls[3].GetComponent<Renderer>().material.mainTexture = ball4;
        balls[4].GetComponent<Renderer>().material.mainTexture = ball5;
        balls[5].GetComponent<Renderer>().material.mainTexture = ball6;
        balls[6].GetComponent<Renderer>().material.mainTexture = ball7;
        balls[7].GetComponent<Renderer>().material.mainTexture = ball8;
        balls[8].GetComponent<Renderer>().material.mainTexture = ball9;
        balls[9].GetComponent<Renderer>().material.mainTexture = ball10;
        balls[10].GetComponent<Renderer>().material.mainTexture = ball11;
        balls[11].GetComponent<Renderer>().material.mainTexture = ball12;
        balls[12].GetComponent<Renderer>().material.mainTexture = ball13;
        balls[13].GetComponent<Renderer>().material.mainTexture = ball14;
        balls[14].GetComponent<Renderer>().material.mainTexture = cueBall;

        balls[14].name = "cue ball";
        balls[14].tag = "cueball";

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
